# Maintainer: Noel Kuntze <noel.kuntze@contauro.com>
pkgname=grommunio-common
pkgver=1.0
pkgrel=2
pkgdesc="Common configuration package for grommunio"
arch="noarch !riscv64"
url="https://grommunio.com/"
license="AGPL-3.0-only"
options="!check" # No test suite
subpackages="$pkgname-openrc"

source="$pkgname-$pkgver.tar.gz::https://github.com/grommunio/configs/archive/refs/tags/v$pkgver.tar.gz
	0001-valkey-grommunio.patch
	0002-nginx-http2.patch

	ssl_certificate.conf
	valkey@grommunio.initd
	valkey@grommunio.confd
	"

builddir="$srcdir/configs-$pkgver"

package() {
	mkdir -p "$pkgdir"
	cp -rp -- * "$pkgdir"

	# service files
	install -dm755 "$pkgdir"/etc/init.d/
	ln -sf "/etc/init.d/valkey" "$pkgdir/etc/init.d/valkey@grommunio"
	install -Dm644 "$srcdir"/valkey@grommunio.confd "$pkgdir"/etc/conf.d/valkey@grommunio

	# fix redis directory
	mv "$pkgdir/etc/redis" "$pkgdir/etc/valkey"

	# fix nginx directory
	mv "$pkgdir/etc/nginx/conf.d" "$pkgdir/etc/nginx/http.d"

	# nginx config file
	install -m644 -D "$srcdir"/ssl_certificate.conf "$pkgdir"/etc/grommunio-common/nginx/ssl_certificate.conf

	# remove unnecessary directories
	rm -r "$pkgdir"/var/lib "$pkgdir"/var/log \
		"$pkgdir"/etc/grommunio-common/nginx/locations.d \
		"$pkgdir"/etc/grommunio-common/nginx/upstreams.d
}

sha512sums="
1e2db173a73012948be3b829919ab3092ba0399228355d7a157398bb5b4b9d6c4150b1f2aaf38cfb857dba710765408a54cf2aab09e369c28d1971a29f221b26  grommunio-common-1.0.tar.gz
32115176de027fe941ebaea4ceef1b9d1fedde6027ec84542e1f40e928f7f6502d65c152d13a31f43c70124d9bc5edb6d5376a3044b58e208ec3c26bea32f574  0001-valkey-grommunio.patch
d6b9a729c32972581936a3d7fe7dbfc62dc5e091306460a04b28fc950301223e7fc93d13702f7124215ba85c32e85bd2233ca6ca99c50e53a049e7fefba272eb  0002-nginx-http2.patch
e2d1a010813078651ca2e728589a1c0c84b6e098c87f59b2ec07d54a9f80b8c9a2168cd853dd73fe07ee6e05cc66f762c2d9ba2511c135314bb1727f6cbfba91  ssl_certificate.conf
192ed1b71c933c750920cf40eb1e277c5f874fc0f87402a3b9696717fc388bb62153a6e0f759c148b510975bc1cc1b84ef06b2df8b7067ebbed33caeb784e383  valkey@grommunio.initd
5d2bb73c96e9455c6a78ec008820b6dda43776f3911f7104f2075fea64ec77434c26eb92ab481c8b325f22b96f60d5ef6685b41fa3ddf5857ffed5ecdd142100  valkey@grommunio.confd
"
