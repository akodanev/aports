# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Krassy Boykinov <kboykinov@teamcentrixx.com>
pkgname=py3-gst
pkgver=1.24.4
pkgrel=0
pkgdesc="GStreamer Python3 bindings"
url="https://gitlab.freedesktop.org/gstreamer/gstreamer"
arch="all"
license="LGPL-2.1-or-later"
depends="py3-gobject3 gst-plugins-base"
makedepends="
	gst-plugins-base-dev
	gstreamer-dev
	meson
	py3-gobject3-dev
	python3-dev
	"
source="
	https://gstreamer.freedesktop.org/src/gst-python/gst-python-$pkgver.tar.xz
	suffix.patch
	"
builddir="$srcdir/gst-python-$pkgver"

prepare() {
	default_prepare

	local pyso="$(readlink /usr/lib/libpython*.*.so)"
	[ -n "$pyso" ]
	msg "libpython: $pyso"
	local suff="${pyso#libpython*.so.}"
	sed -i "s|@SUFF@|$suff|" meson.build
}

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dpython=/usr/bin/python3 \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
322ec6f6d0e16b97682e8be7e6614d09bb6718fcc71520487359278a31e0e56470d9744c358fa2f7d4052d84d3f8142d6f0bf84879999beb50afb1684bc73957  gst-python-1.24.4.tar.xz
5ec14d7fa8bb39e5a07ca7c34ee9e383ba1677e0f3a8d84987eb8e1d062fd46b3cc4663fa99f2291684a169eda8681ab47b1361cd4f3e8128a5cd5aa4ef0c3ab  suffix.patch
"
